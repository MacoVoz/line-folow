//#define JOY_MAX 40   
#define PWMA 23
#define PWMB 17
#define AIN1 21
#define AIN2 22
#define BIN1 19
#define BIN2 18
#define STBY 20

#define f_speed 100
#define min_speed 50
#define max_speed 150
#define DEL 50

bool sensors[6] = {0, 0, 0, 0, 0, 0};

int err;
void forward();

void right();
void left();
void readd();
void motor();

void setup(){

  pinMode(PWMA, OUTPUT);  
  pinMode(PWMB, OUTPUT);  
  pinMode(AIN1, OUTPUT);  
  pinMode(AIN2, OUTPUT);  
  pinMode(BIN1, OUTPUT);  
  pinMode(BIN2, OUTPUT);  
  pinMode(STBY, OUTPUT);
  pinMode(0, INPUT);
  pinMode(1, INPUT);
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(5, INPUT);

  
  Serial.begin(9600);
}

void loop()
{
  sensors[0]=digitalRead(0);
  sensors[1]=digitalRead(1);
  sensors[2]=digitalRead(2);
  sensors[3]=digitalRead(3);
  sensors[4]=digitalRead(4);
  sensors[5]=digitalRead(5);

  Serial.print(sensors[0]);
  Serial.print(" ");
  Serial.print(sensors[1]);
  Serial.print(" ");
  Serial.print(sensors[2]);
  Serial.print(" ");
  Serial.print(sensors[3]);
  Serial.print(" ");
  Serial.print(sensors[4]);
  Serial.print(" ");
  Serial.print(sensors[5]);
  Serial.println();

  // forward();
  // delay(1000);
  // left();
  // delay(1000);
  // right();
  // delay(1000);
  if(sensors[2] == 1 && sensors[3] == 1)
    forward();
  if (sensors[2] == 1)
    forward();
  if(sensors[3] == 1)
    forward();
  if(sensors[0] == 1){
    left();
    delay(DEL);
  }
  if(sensors[1] == 1)
    left();
  if(sensors[4] == 1)
    right();
  if(sensors[5] == 1){
    right();
    delay(DEL);
  }




}


void forward()
{
  digitalWrite(STBY, HIGH); 
  digitalWrite(AIN1, LOW); 
  digitalWrite(AIN2, HIGH); 
  analogWrite(PWMA, f_speed);
  digitalWrite(BIN1, HIGH); 
  digitalWrite(BIN2, LOW); 
  analogWrite(PWMB, f_speed);
}

void right()
{
  digitalWrite(STBY, HIGH); 
  digitalWrite(AIN1, LOW); 
  digitalWrite(AIN2, HIGH); 
  analogWrite(PWMA, max_speed);
  digitalWrite(BIN1, HIGH); 
  digitalWrite(BIN2, LOW); 
  analogWrite(PWMB, min_speed);
}

void left()
{
  digitalWrite(STBY, HIGH); 
  digitalWrite(AIN1, LOW); 
  digitalWrite(AIN2, HIGH); 
  analogWrite(PWMA, min_speed);
  digitalWrite(BIN1, HIGH); 
  digitalWrite(BIN2, LOW); 
  analogWrite(PWMB, max_speed);
}

