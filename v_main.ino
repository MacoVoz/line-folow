#define JOY_MAX 40   
#define PWMA 11 
#define PWMB 10
#define AIN1 7
#define AIN2 6
#define BIN1 4
#define BIN2 5
#define STBY 13 

const int switchPin = 3;
const int pinX = A8;
const int pinY = A9;

float err;
int sensor[5]={0,0,0,0,0};
int speedl=90;
int speedr=90;
int i=1;

void readd(void);
void motor(void);
void joy(void);
void forward(void);
void back(void);
void left(void);
void right(void);

void setup(){

    pinMode(PWMA, OUTPUT);  
    pinMode(PWMB, OUTPUT);  
    pinMode(AIN1, OUTPUT);  
    pinMode(AIN2, OUTPUT);  
    pinMode(BIN1, OUTPUT);  
    pinMode(BIN2, OUTPUT);  
    pinMode(STBY, OUTPUT);  
    digitalWrite(STBY, HIGH); 
    pinMode(pinX, INPUT);
    pinMode(pinY, INPUT);
    pinMode(switchPin, INPUT);
    digitalWrite(switchPin, HIGH);
    Serial.begin(9600);
}

void loop()
{
  int buttom = digitalRead(switchPin);
  if (buttom==0){
      i=i+1;
    }
  if (i%2==0){
      joy();
      motor();
    }
   else{
      readd();
      motor();
    }

}

void joy(){
  int X = analogRead(pinX); 
  int Y = analogRead(pinY);
  if(X>=900){
    err=7;
    }
   else if(X<=50){
    err=8;
    }
   else if(Y<=450){
    err=0;
    }
    else if(Y>=550){
    err=6;
    }
    else if(450<Y<550 & 50<X<900){
    err=5;
    }
  }

void readd()
{
  sensor[1]=digitalRead(A0);
  sensor[2]=digitalRead(A1);
  sensor[3]=digitalRead(A2);
  sensor[4]=digitalRead(A3);
  sensor[5]=digitalRead(A4);
  if((sensor[1]==1)&&(sensor[2]==0)&&(sensor[3]==0)&&(sensor[4]==0)&&(sensor[5]==0))
err=2;
else if((sensor[1]==0)&&(sensor[2]==1)&&(sensor[3]==0)&&(sensor[4]==0)&&(sensor[5]==0))
  err=1;
  else if((sensor[1]==0)&&(sensor[2]==0)&&(sensor[3]==1)&&(sensor[4]==0)&&(sensor[5]==0))
err=0;

  else if((sensor[1]==0)&&(sensor[2]==0)&&(sensor[3]==0)&&(sensor[4]==1)&&(sensor[5]==0))
err=3;
  else if((sensor[1]==0)&&(sensor[2]==0)&&(sensor[3]==0)&&(sensor[4]==0)&&(sensor[5]==1))
err=4;
}


void forward(){
    digitalWrite(7,HIGH);
    digitalWrite(6,LOW);
    digitalWrite(5,LOW);
    digitalWrite(4,HIGH);
  }
void left(){
    digitalWrite(7,LOW);
    digitalWrite(6,HIGH);
    digitalWrite(5,LOW);
    digitalWrite(4,HIGH);
  }
  
void right(){
    digitalWrite(7,HIGH);
    digitalWrite(6,LOW);
    digitalWrite(5,HIGH);
    digitalWrite(4,LOW);
  }
void back(){
    digitalWrite(7,LOW);
    digitalWrite(6,HIGH);
    digitalWrite(5,HIGH);
    digitalWrite(4,LOW);
  }  


void motor()
{
  if(err==0){
    analogWrite(11,240);
    analogWrite(10,240); 
    forward();
    }
   else if(err==3){
    analogWrite(11,255);
    analogWrite(10,15); 
    forward();
    }
   else if(err==1){
    analogWrite(11,15);
    analogWrite(10,255); 
    forward();
    }
   else if(err==4){
    analogWrite(11,250);
    analogWrite(10,90); 
    right();
    }
   else if(err==2){
    analogWrite(11,90);
    analogWrite(10,250); 
    left();
    }
   else if(err==5){
    analogWrite(11,0);
    analogWrite(10,0); 
    forward();
    }
    else if(err==6){
    analogWrite(11,140);
    analogWrite(10,140);
    back();
    }
   else if(err==7){
    analogWrite(11,150);
    analogWrite(10,0); 
    right();
    }
   else if(err==8){
    analogWrite(11,0);
    analogWrite(10,150); 
    left();
    }

}