byte PWMAPin = 5;
byte PWMBPin = 9;

byte AIN2Pin = 3;
byte AIN1Pin = 7;


byte BIN2Pin = 11;
byte BIN1Pin = 10;

byte STBPin = 6;
byte vcc = 2;


byte lineSensor1Pin = 12;
byte lineSensor2Pin = 8;
byte lineSensor3Pin = 4;


// значення деяких змінних

byte pwmA = 225;
byte pwmB = 250;


boolean lineSensor1;
boolean lineSensor2;
boolean lineSensor3;




void setup() {
  // Serial.begin(9600);
  // put your setup code here, to run once:
//  // Налаштування піна
 pinMode(PWMAPin, OUTPUT);
 pinMode(PWMBPin, OUTPUT);
 pinMode(AIN2Pin, OUTPUT);
 pinMode(AIN1Pin, OUTPUT);
 pinMode(BIN2Pin, OUTPUT);
 pinMode(BIN1Pin, OUTPUT);
 pinMode(STBPin, OUTPUT);
 pinMode(vcc, OUTPUT);

////  // подаєм сигнал на пін
 digitalWrite(AIN2Pin, 1);
 digitalWrite(AIN1Pin, 0);
 digitalWrite(BIN2Pin, 1);
 digitalWrite(BIN1Pin, 0);
 digitalWrite(STBPin, 1);
 digitalWrite(vcc, 1);

 analogWrite(PWMAPin, pwmA);
 analogWrite(PWMBPin, pwmB);

}

void STOP(/* arguments */) {
  digitalWrite(AIN2Pin, 0);
  digitalWrite(AIN1Pin, 1);
  digitalWrite(BIN2Pin, 0);
  digitalWrite(BIN1Pin, 1);
  analogWrite(PWMAPin, 255);
  analogWrite(PWMBPin, 255);
  delay(5);
  analogWrite(PWMAPin, 0);
  analogWrite(PWMBPin, 0);
  digitalWrite(AIN2Pin, 1);
  digitalWrite(AIN1Pin, 0);
  digitalWrite(BIN2Pin, 1);
  digitalWrite(BIN1Pin, 0);
  digitalWrite(STBPin, 1);
}

void loop() {


  analogWrite(PWMAPin, pwmA);
  analogWrite(PWMBPin, pwmB);
  delay(1000);
  STOP();
  delay(1000);


}
